import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionDemo {
	
	public static void main(String[] args) {
		//Generics
		//Generic collection Allowes only one type of object in the collection
		//Before Generic we can create ANY type of object in collection
		//now Generic store only one spicific type of object
	List<String> list =new ArrayList<String>();
		
		list.add("Mango");
		list.add("Apple");
		list.add("Orange");
		list.add("Lemon");
		list.add("Mango");
		
		System.out.println(list);
		
	Set<Integer> set=new HashSet<Integer>();
		
		
		set.add(10);
		set.add(40);
		set.add(70);
		set.add(10);
		set.add(80);
		
		System.out.println(set);
		
	List<Float> l=new ArrayList<Float>();
	
	      l.add(10.00f);
	      l.add(80.00f);
	      l.add(10.00f);
	      l.add(90.00f);
	      l.add(100.00f);
	      
	      System.out.println("Size:" +l.size());
	      
	      System.out.println(l);
	      
		
		
	}
}
