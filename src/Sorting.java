import java.util.Arrays;
import java.util.HashSet;


public class Sorting {
	public static void main(String[] args) {
		
		String[] employee = {"Anees","Rehaman","Dhoni","Anees"};
		Integer[]salary   = {12000, 15000, 16000,12300};
		
		Arrays.sort(employee);
		Arrays.sort(salary);
		
		HashSet set =new  HashSet();
		set.add("Anees,12000");
		set.add("Rehaman,15000");
		set.add("Dhoni,16000");
		set.add("Anees,12300");
		
		
		System.out.println(Arrays.toString(employee));
		System.out.println(Arrays.toString(salary));
		System.out.println("employee,salary:"+ set);
	}

}
